EESchema Schematic File Version 4
LIBS:LedRings-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED:CQY99 D1
U 1 1 64248870
P 2200 1750
F 0 "D1" H 2150 2040 50  0000 C CNN
F 1 "CQY99" H 2150 1949 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 2200 1925 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 2150 1750 50  0001 C CNN
	1    2200 1750
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D2
U 1 1 642488D4
P 2100 2250
F 0 "D2" H 2050 2540 50  0000 C CNN
F 1 "CQY99" H 2050 2449 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 2100 2425 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 2050 2250 50  0001 C CNN
	1    2100 2250
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D3
U 1 1 6424896E
P 2150 2750
F 0 "D3" H 2100 3040 50  0000 C CNN
F 1 "CQY99" H 2100 2949 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 2150 2925 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 2100 2750 50  0001 C CNN
	1    2150 2750
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D4
U 1 1 642489B0
P 2150 3200
F 0 "D4" H 2100 3490 50  0000 C CNN
F 1 "CQY99" H 2100 3399 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 2150 3375 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 2100 3200 50  0001 C CNN
	1    2150 3200
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D5
U 1 1 642489F8
P 2150 3700
F 0 "D5" H 2100 3990 50  0000 C CNN
F 1 "CQY99" H 2100 3899 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 2150 3875 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 2100 3700 50  0001 C CNN
	1    2150 3700
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D6
U 1 1 64248A44
P 2150 4200
F 0 "D6" H 2100 4490 50  0000 C CNN
F 1 "CQY99" H 2100 4399 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 2150 4375 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 2100 4200 50  0001 C CNN
	1    2150 4200
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D7
U 1 1 64248AA8
P 3300 1750
F 0 "D7" H 3250 2040 50  0000 C CNN
F 1 "CQY99" H 3250 1949 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 3300 1925 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 3250 1750 50  0001 C CNN
	1    3300 1750
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D8
U 1 1 64248AFA
P 3350 2250
F 0 "D8" H 3300 2540 50  0000 C CNN
F 1 "CQY99" H 3300 2450 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 3350 2425 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 3300 2250 50  0001 C CNN
	1    3350 2250
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D9
U 1 1 64248B38
P 3300 2750
F 0 "D9" H 3250 3040 50  0000 C CNN
F 1 "CQY99" H 3250 2949 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 3300 2925 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 3250 2750 50  0001 C CNN
	1    3300 2750
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D10
U 1 1 64248B8A
P 3300 3200
F 0 "D10" H 3250 3490 50  0000 C CNN
F 1 "CQY99" H 3250 3399 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 3300 3375 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 3250 3200 50  0001 C CNN
	1    3300 3200
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D11
U 1 1 64248BC6
P 3350 3650
F 0 "D11" H 3300 3940 50  0000 C CNN
F 1 "CQY99" H 3300 3849 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 3350 3825 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 3300 3650 50  0001 C CNN
	1    3350 3650
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 D12
U 1 1 64248C50
P 3350 4150
F 0 "D12" H 3300 4440 50  0000 C CNN
F 1 "CQY99" H 3300 4349 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 3350 4325 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 3300 4150 50  0001 C CNN
	1    3350 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR0101
U 1 1 64248DFD
P 3100 4950
F 0 "#PWR0101" H 3100 4700 50  0001 C CNN
F 1 "GNDREF" H 3105 4777 50  0000 C CNN
F 2 "" H 3100 4950 50  0001 C CNN
F 3 "" H 3100 4950 50  0001 C CNN
	1    3100 4950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 64248E68
P 2700 1450
F 0 "#PWR0102" H 2700 1300 50  0001 C CNN
F 1 "+3.3V" H 2715 1623 50  0000 C CNN
F 2 "" H 2700 1450 50  0001 C CNN
F 3 "" H 2700 1450 50  0001 C CNN
	1    2700 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 64248ECC
P 2200 1050
F 0 "#PWR0103" H 2200 800 50  0001 C CNN
F 1 "GND" H 2205 877 50  0000 C CNN
F 2 "" H 2200 1050 50  0001 C CNN
F 3 "" H 2200 1050 50  0001 C CNN
	1    2200 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1450 2700 1750
Wire Wire Line
	2700 1750 2300 1750
Wire Wire Line
	2700 2250 2200 2250
Connection ~ 2700 1750
Wire Wire Line
	2700 1750 2700 2250
Wire Wire Line
	2250 2750 2700 2750
Wire Wire Line
	2700 2750 2700 2250
Connection ~ 2700 2250
Wire Wire Line
	2250 3200 2700 3200
Wire Wire Line
	2700 3200 2700 2750
Connection ~ 2700 2750
Wire Wire Line
	2250 3700 2700 3700
Wire Wire Line
	2700 3700 2700 3200
Connection ~ 2700 3200
Wire Wire Line
	2250 4200 2700 4200
Wire Wire Line
	2700 4200 2700 3700
Connection ~ 2700 3700
Wire Wire Line
	2700 4200 2700 4450
Wire Wire Line
	2700 4450 3750 4450
Wire Wire Line
	3750 4450 3750 4150
Wire Wire Line
	3750 4150 3450 4150
Connection ~ 2700 4200
Wire Wire Line
	3750 4150 3750 3650
Wire Wire Line
	3750 3650 3450 3650
Connection ~ 3750 4150
Wire Wire Line
	3400 3200 3750 3200
Wire Wire Line
	3750 3200 3750 3650
Connection ~ 3750 3650
Wire Wire Line
	3400 2750 3750 2750
Wire Wire Line
	3750 2750 3750 3200
Connection ~ 3750 3200
Wire Wire Line
	3450 2250 3750 2250
Wire Wire Line
	3750 2250 3750 2750
Connection ~ 3750 2750
Wire Wire Line
	3750 1750 3750 2250
Wire Wire Line
	2200 1050 1650 1050
Wire Wire Line
	1650 1050 1650 1750
Wire Wire Line
	1650 1750 2000 1750
Wire Wire Line
	1650 1750 1650 2250
Wire Wire Line
	1650 2250 1900 2250
Connection ~ 1650 1750
Wire Wire Line
	1650 2250 1650 2750
Wire Wire Line
	1650 2750 1950 2750
Connection ~ 1650 2250
Wire Wire Line
	1650 2750 1650 3200
Wire Wire Line
	1650 3200 1950 3200
Connection ~ 1650 2750
Wire Wire Line
	1650 3200 1650 3700
Wire Wire Line
	1650 3700 1950 3700
Connection ~ 1650 3200
Wire Wire Line
	1650 3700 1650 4200
Wire Wire Line
	1650 4200 1950 4200
Connection ~ 1650 3700
Wire Wire Line
	2200 1050 2900 1050
Wire Wire Line
	2950 1050 2950 1750
Wire Wire Line
	2950 1750 3100 1750
Connection ~ 2200 1050
Wire Wire Line
	2950 1750 2950 2250
Wire Wire Line
	2950 2250 3150 2250
Connection ~ 2950 1750
Wire Wire Line
	2950 2250 2950 2750
Wire Wire Line
	2950 2750 3100 2750
Connection ~ 2950 2250
Wire Wire Line
	2950 2750 2950 3200
Wire Wire Line
	2950 3200 3100 3200
Connection ~ 2950 2750
Wire Wire Line
	2950 3200 2950 3650
Wire Wire Line
	2950 3650 3150 3650
Connection ~ 2950 3200
Wire Wire Line
	2950 3650 2950 4150
Wire Wire Line
	2950 4150 3150 4150
Connection ~ 2950 3650
Connection ~ 3750 2250
$Comp
L Connector:TestPoint TP_GND1
U 1 1 64256D72
P 2900 900
F 0 "TP_GND1" H 2958 1020 50  0000 L CNN
F 1 "TestPoint" H 2958 929 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 3100 900 50  0001 C CNN
F 3 "~" H 3100 900 50  0001 C CNN
	1    2900 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP_3.3V1
U 1 1 64256DED
P 3750 1500
F 0 "TP_3.3V1" H 3808 1620 50  0000 L CNN
F 1 "TestPoint" H 3808 1529 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 3950 1500 50  0001 C CNN
F 3 "~" H 3950 1500 50  0001 C CNN
	1    3750 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 900  2900 1050
Connection ~ 2900 1050
Wire Wire Line
	2900 1050 2950 1050
Wire Wire Line
	3750 1500 3750 1750
Connection ~ 3750 1750
$Comp
L LED:CQY99 DI1
U 1 1 6425FB6C
P 5500 2250
F 0 "DI1" H 5450 2540 50  0000 C CNN
F 1 "CQY99" H 5450 2449 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 5500 2425 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 5450 2250 50  0001 C CNN
	1    5500 2250
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 DI2
U 1 1 6425FB73
P 5550 2750
F 0 "DI2" H 5500 3040 50  0000 C CNN
F 1 "CQY99" H 5500 2950 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 5550 2925 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 5500 2750 50  0001 C CNN
	1    5550 2750
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 DI3
U 1 1 6425FB7A
P 5500 3250
F 0 "DI3" H 5450 3540 50  0000 C CNN
F 1 "CQY99" H 5450 3449 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 5500 3425 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 5450 3250 50  0001 C CNN
	1    5500 3250
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 DI4
U 1 1 6425FB81
P 5500 3700
F 0 "DI4" H 5450 3990 50  0000 C CNN
F 1 "CQY99" H 5450 3899 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 5500 3875 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 5450 3700 50  0001 C CNN
	1    5500 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3700 5950 3700
Wire Wire Line
	5600 3250 5950 3250
Wire Wire Line
	5150 3250 5300 3250
Wire Wire Line
	5600 2250 5950 2250
Wire Wire Line
	3700 1750 3750 1750
Wire Wire Line
	3400 1750 3750 1750
$Comp
L Connector:TestPoint TP_GND_inner1
U 1 1 64260E46
P 4800 850
F 0 "TP_GND_inner1" H 4858 970 50  0000 L CNN
F 1 "TestPoint" H 4858 879 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 5000 850 50  0001 C CNN
F 3 "~" H 5000 850 50  0001 C CNN
	1    4800 850 
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP_3.3V_inner1
U 1 1 64260E4D
P 5650 1450
F 0 "TP_3.3V_inner1" H 5708 1570 50  0000 L CNN
F 1 "TestPoint" H 5708 1479 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 5850 1450 50  0001 C CNN
F 3 "~" H 5850 1450 50  0001 C CNN
	1    5650 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 850  4800 1000
Wire Wire Line
	4800 1000 4850 1000
Wire Wire Line
	4850 2250 5300 2250
Wire Wire Line
	4850 1000 4850 2250
Wire Wire Line
	4850 2750 4850 2250
Connection ~ 4850 2250
Wire Wire Line
	4850 2750 5150 2750
Connection ~ 5150 2750
Wire Wire Line
	5150 2750 5350 2750
Wire Wire Line
	5650 1850 5950 1850
Wire Wire Line
	5950 1850 5950 2250
Wire Wire Line
	5650 1450 5650 1850
Wire Wire Line
	5650 2750 5950 2750
Wire Wire Line
	5950 2750 5950 2250
Connection ~ 5950 2250
Connection ~ 5950 2750
Connection ~ 5950 3250
Wire Wire Line
	5950 3700 5950 3250
Wire Wire Line
	5950 3250 5950 2750
Connection ~ 5150 3250
Wire Wire Line
	5150 3700 5150 3250
Wire Wire Line
	5150 3250 5150 2750
$Comp
L LED:CQY99 DI5
U 1 1 6426D2CF
P 5550 4250
F 0 "DI5" H 5500 4540 50  0000 C CNN
F 1 "CQY99" H 5500 4449 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 5550 4425 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 5500 4250 50  0001 C CNN
	1    5550 4250
	1    0    0    -1  
$EndComp
$Comp
L LED:CQY99 DI6
U 1 1 6426D2D6
P 5550 4700
F 0 "DI6" H 5500 4990 50  0000 C CNN
F 1 "CQY99" H 5500 4899 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_IRGrey" H 5550 4875 50  0001 C CNN
F 3 "https://www.prtice.info/IMG/pdf/CQY99.pdf" H 5500 4700 50  0001 C CNN
	1    5550 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4700 6000 4700
Wire Wire Line
	5200 4250 5350 4250
Wire Wire Line
	5200 4700 5350 4700
Wire Wire Line
	5650 4250 6000 4250
Wire Wire Line
	6000 4250 6000 3700
Wire Wire Line
	6000 3700 5950 3700
Connection ~ 5950 3700
Wire Wire Line
	6000 4700 6000 4250
Connection ~ 6000 4250
Wire Wire Line
	5200 4250 5200 3700
Wire Wire Line
	5150 3700 5200 3700
Connection ~ 5200 3700
Wire Wire Line
	5200 3700 5300 3700
Wire Wire Line
	5200 4700 5200 4250
Connection ~ 5200 4250
$EndSCHEMATC
